# Задание №1 -- Добавить на страницу слайдер

## Шаги решения задания:

1) Добавить участок кода после блока с меню:
    ```html 
   <div class="slider" id="slider">
        <div><img src="file.jpg" alt="Слайд 1"></div>
        <div><img src="file.jpg" alt="Слайд 2"></div>
        <div><img src="file.jpg" alt="Слайд 3"></div>
    </div>
   ``` 
   
2) Найти в гугле Slick и перейти на страницу проекта
3) Показать демо на сайте
4) Перейти в пункт "get it now"
5) Обратить внимание на ссылки css и js
6) Добавить стили библиотеки slick: 
    ```html
   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
   ```
   
7) Добавить скрипты библиотеки slick:
    ```html
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
   ```
   
8) Добавить библиотеку JQuery:
    ```html
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.7.0.js"></script>
    ```
   
9) Добавить скрипты создания слайдера
    ```javascript
    $(document).ready(function() {
        $('#slider').slick({
            infinite: true,
            prevArrow: '<div class="slick-btn-own slick-prev">&#8592;</div>',
            nextArrow: '<div class="slick-btn-own slick-next">&#8594;</div>'
        });
    });
    ```
10) Добавить стили для слайдера:
    ```css
    .slider {
        width: 100%;
        margin-bottom: 20px;
    }
    
    .slider img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
    
    .slick-btn-own {
        position: absolute;
        z-index: 999;
        top: calc(50% - 15px);
        cursor: pointer;
        width: 30px;
        height: 30px;
        background: #FFFFFF;
        box-shadow: 0 0 3px rgba(0, 0, 0, 0.3);
        transition: box-shadow 100ms linear;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    
    .slick-btn-own:hover {
        box-shadow: 0 0 4px rgba(0, 0, 0, 0.5);
    }
    
    .slick-btn-own:active {
        box-shadow: 0 0 3px rgba(0, 0, 0, 0.2);
    }
    
    .slick-prev {
        left: 15px;
    }
    
    .slick-next {
        right: 15px;
    }
    ```
