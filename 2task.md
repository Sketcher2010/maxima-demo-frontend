# Задание №2 -- Добавить на страницу форму обратной связи

## Шаги решения задания:

1) Добавить участок кода после блока с новостями:
    ```html
    <div class="feedback-form">
        <h2>Обратная связь</h2>
        <form action="#" method="POST">
            <label for="name">Имя:</label>
            <input type="text" id="name" name="name" required>

            <label for="email">Email:</label>
            <input type="email" id="email" name="email" required>

            <label for="message">Сообщение:</label>
            <textarea id="message" name="message" required></textarea>

            <input type="submit" value="Отправить">
        </form>
    </div>
   ```
2) Добавить CSS стили:
    ```css
   .feedback-form {
        background-color: #ffffff;
        padding: 20px;
        border-radius: 4px;
        margin-top: 20px;
    }
    
    .feedback-form h2 {
        font-size: 24px;
        margin-bottom: 20px;
    }
    
    .feedback-form label {
        display: block;
        margin-bottom: 10px;
    }
    
    .feedback-form input,
    .feedback-form textarea {
        width: 100%;
        padding: 10px;
        border-radius: 4px;
        border: 1px solid #dddddd;
        margin-bottom: 10px;
        box-sizing: border-box;
    }
    
    .feedback-form input[type="submit"] {
        background-color: #555555;
        color: #ffffff;
        border: none;
        padding: 10px 20px;
        border-radius: 4px;
        cursor: pointer;
    }
    
    .feedback-form input[type="submit"]:hover,
    .feedback-form input[type="submit"]:focus,
    .feedback-form input[type="submit"]:active {
        background-color: #333333;
    }
   ```
