# Задание №3 -- Добавить на страницу модальное окно создания пользователя

## Шаги решения задания:

1) Добавить код после `<body>`
    ```html
   <div id="user-modal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <h2>Создание пользователя</h2>
            <form id="user-form" action="#" method="POST">
                <label for="fullname">ФИО:</label>
                <input type="text" id="fullname" name="fullname" required>

                <label for="phone">Телефон:</label>
                <input type="tel" id="phone" name="phone" required>

                <label for="user-email">Email:</label>
                <input type="email" id="user-email" name="user-email" required>

                <label for="username">Логин:</label>
                <input type="text" id="username" name="username" required>

                <label for="generated-password">Пароль:</label>
                <input type="password" id="generated-password" name="generated-password" required>

                <label for="user-group">Группа пользователя:</label>
                <select id="user-group" name="user-group" required>
                    <option value="">Выберите группу</option>
                    <option value="admin">Администратор</option>
                    <option value="user">Пользователь</option>
                </select>

                <input type="submit" value="Создать">
            </form>
        </div>
    </div>
   ```

2) Добавить css стили:
    ```css
   .modal {
        display: none; /* block если надо тестировать */
        position: fixed;
        z-index: 1000;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5);
    }
    
    .modal-content {
        background-color: #ffffff;
        margin: 10% auto;
        padding: 20px;
        border-radius: 4px;
        width: 400px;
    }
    
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
        cursor: pointer;
    }
    
    .close:hover,
    .close:focus {
        color: #000000;
        text-decoration: none;
        cursor: pointer;
    }
    
    .modal h2 {
        font-size: 24px;
        margin-bottom: 20px;
    }
    
    .modal label {
        display: block;
        margin-bottom: 10px;
    }
    
    .modal input,
    .modal select {
        width: 100%;
        padding: 10px;
        border-radius: 4px;
        border: 1px solid #dddddd;
        margin-bottom: 10px;
        box-sizing: border-box;
    }
    
    .modal input[type="submit"] {
        background-color: #555555;
        color: #ffffff;
        border: none;
        padding: 10px 20px;
        border-radius: 4px;
        cursor: pointer;
        box-sizing: border-box;
    }
    
    .modal input[type="submit"]:hover,
    .modal input[type="submit"]:focus,
    .modal input[type="submit"]:active {
        background-color: #333333;
    }
    
    .error {
        color: red;
        font-size: 12px;
        margin-top: 5px;
    }
   ```

3) Добавить JS код внутрь `$(document).ready()`
    ```javascript
   let modal = $('#user-modal');
   let btn = $('#create-user-btn');
   let span = $('.close');
   
   btn.click(function() {
      modal.css({display: 'block'});
   });
   
   span.click(function() {
      modal.css({display: 'none'});
   });
   
   window.onclick = function(event) {
      if (event.target == modal) {
          modal.css({display: 'none'});
      }
   }
   
   // Валидация формы
   let userForm = document.getElementById('user-form');
   let fullnameInput = document.getElementById('fullname');
   let phoneInput = document.getElementById('phone');
   let emailInput = document.getElementById('user-email');
   let usernameInput = document.getElementById('username');
   let generatedPasswordInput = document.getElementById('generated-password');
   let userGroupInput = document.getElementById('user-group');
   
   userForm.addEventListener('submit', function(event) {
      let isValid = true;
   
      if (fullnameInput.value.trim() === '') {
          showError(fullnameInput, 'Пожалуйста, введите ФИО');
          isValid = false;
      } else {
          hideError(fullnameInput);
      }
   
      if (phoneInput.value.trim() === '') {
          showError(phoneInput, 'Пожалуйста, введите номер телефона');
          isValid = false;
      } else {
          hideError(phoneInput);
      }
   
      if (emailInput.value.trim() === '') {
          showError(emailInput, 'Пожалуйста, введите Email');
          isValid = false;
      } else {
          hideError(emailInput);
      }
   
      if (usernameInput.value.trim() === '') {
          showError(usernameInput, 'Пожалуйста, введите логин');
          isValid = false;
      } else {
          hideError(usernameInput);
      }
   
      if (generatedPasswordInput.value.trim() === '') {
          showError(generatedPasswordInput, 'Пожалуйста, введите пароль');
          isValid = false;
      } else {
          hideError(generatedPasswordInput);
      }
   
      if (userGroupInput.value === '') {
          showError(userGroupInput, 'Пожалуйста, выберите группу пользователя');
          isValid = false;
      } else {
          hideError(userGroupInput);
      }
   
      if (!isValid) {
          event.preventDefault();
      }
   });
   
   function showError(input, message) {
      let errorElement = document.createElement('div');
      errorElement.className = 'error';
      errorElement.innerHTML = message;
      input.parentNode.appendChild(errorElement);
   }
   
   function hideError(input) {
      let errorElement = input.parentNode.querySelector('.error');
      if (errorElement) {
          errorElement.parentNode.removeChild(errorElement);
      }
   }
    ```
